import mongoose from 'mongoose';
import bcrypt from 'bcrypt';


const SALT_WORK_FACTOR = 10;

const Schema = mongoose.Schema;


const UserSchema = new Schema(  {

    'name': {
        type : String,
        required: true
    },
    'lastName': {
        type: String,
        required: true
    },
    'email':{
        type: String,
        required: true
    },
    'password':{
        type: String,
        required: true
    },

}

,

{'collection': 'users', timestamps: true});



UserSchema.pre('save', function(next){
    var user = this;

    // solo modificamos la contraseñña si se ha modificado o es nueva
    if(!user.isModified('password')) return next();

    //GENERAMOS UN SALT
    bcrypt.genSalt(SALT_WORK_FACTOR, function(err,salt){
       if(err){
           console.log(err)
       }

        //hasheamos la contraseña usando el nuevo salt
        bcrypt.hash(user.password, salt, function(err, hash){
            if (err){
                console.log(err)
            }

            //hacemos un override de la contraseña con la nueva hasheada
            user.password = hash;
            next();
        })
    })
});

UserSchema.methods.comparePassword = function(candidatePassword, cb){
    bcrypt.compare(candidatePassword, this.password, function(err, isMatch){
        if(err) return cb(err);
        cb(null,isMatch);
    });
};


export default mongoose.model('users',  UserSchema);