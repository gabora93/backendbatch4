import addMovies from './add';
import updateMovies from './update';
import deleteMovies from './delete';
import addRank from './addRank';

export default {
    addMovies,
    updateMovies,
    deleteMovies,
    addRank

}